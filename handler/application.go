package handler

import (
	"context"
	"ems/application-service/facade"
	"ems/application-service/validator"
	application "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
	log "github.com/micro/go-micro/v2/logger"
)

type ApplicationHandler struct{
	ApplicationFacade facade.IApplicationFacade
	ApplicationApprovalFacade facade.IApplicationApprovalFacade
	ApplicationMemberFacade facade.IApplicationMemberFacade
	ApplicationBudgetFacade facade.IApplicationBudgetFacade
	ApplicationStageFacade facade.IApplicationStageFacade
	ApplicationContentFacade facade.IApplicationContentFacade
	authSrvClientUserService authPB.UserService
	authSrvClientRolePermission authPB.RolePermissionService
	eventSrvClientEvent eventPB.EventService
	authSrvClientUserAssignEventRoleService authPB.UserAssignEventRoleService
}


func NewApplicationHandler(ApplicationFacade facade.IApplicationFacade,
							ApplicationApprovalFacade facade.IApplicationApprovalFacade,
							ApplicationMemberFacade facade.IApplicationMemberFacade,
							ApplicationBudgetFacade facade.IApplicationBudgetFacade,
							ApplicationStageFacade facade.IApplicationStageFacade,
							ApplicationContentFacade facade.IApplicationContentFacade,
							authSrvClientUserService authPB.UserService,
							authSrvClientRolePermission authPB.RolePermissionService,
							eventSrvClientEvent eventPB.EventService,
							authSrvClientUserAssignEventRoleService authPB.UserAssignEventRoleService) *ApplicationHandler {
	return &ApplicationHandler{
		ApplicationFacade: ApplicationFacade,
		ApplicationApprovalFacade: ApplicationApprovalFacade,
		authSrvClientUserService: authSrvClientUserService,
		ApplicationMemberFacade: ApplicationMemberFacade,
		ApplicationBudgetFacade: ApplicationBudgetFacade,
		ApplicationContentFacade: ApplicationContentFacade,
		ApplicationStageFacade: ApplicationStageFacade,
		authSrvClientRolePermission: authSrvClientRolePermission,
		eventSrvClientEvent: eventSrvClientEvent,
		authSrvClientUserAssignEventRoleService: authSrvClientUserAssignEventRoleService,
	}
}

func (a *ApplicationHandler) GetApplicationDetails(ctx context.Context, request *application.ViewApplicationDetailsRequest, response *application.ViewApplicationDetailsResponse) error {
	res, err := a.ApplicationFacade.GetApplicationById(ctx, a.authSrvClientUserService, a.authSrvClientRolePermission, request.ApplicationId)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Title = res.Title
	response.CreatedBy = res.CreatedBy
	response.Approver = res.Approver
	response.Stages = res.Stages
	response.EndDate = res.EndDate
	response.StartDate = res.StartDate
	response.Description = res.Description
	response.Budgets = res.Budgets
	response.Members = res.Members
	response.Scale = res.Scale
	response.Status = res.Status
	response.Id = res.Id
	return nil
}

func (a *ApplicationHandler) ChangeApplicationStatus(ctx context.Context, request *application.ChangeApplicationStatusRequest, response *application.ChangeApplicationStatusResponse) error {
	err := a.ApplicationApprovalFacade.AddApplicationApproval(ctx, a.authSrvClientUserService, a.authSrvClientUserAssignEventRoleService, a.authSrvClientRolePermission, a.eventSrvClientEvent, request.ApplicationId, request.Status)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Success = true
	return nil
}

func (a *ApplicationHandler) UpdateApplication(ctx context.Context, request *application.UpdateApplicationRequest, response *application.UpdateApplicationResponse) error {
	err := validator.ApplicationValidator(&request.Title, &request.Description, &request.Scale)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	startDate, endDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, nil, nil)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	for _, member := range request.Members {
		err := validator.ApplicationMemberValidator(&member.MemberId, &member.EventRoleId)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, budget := range request.Budgets {
		err := validator.ApplicationBudgetValidator(&budget.Description, &budget.UnitPrice, &budget.Amount, &budget.TotalPrice)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, stage := range request.Stages {
		err := validator.ApplicationStageValidator(&stage.Name, &stage.Location, &stage.Description)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		stageStartDate, stageEndDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		for _, content := range stage.Contents {
			err := validator.ApplicationContentValidator(&content.Name, &content.Description, &content.LeaderId)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			_, _, err = validator.StartDateEndDateValidator(request.StartDate, request.EndDate, stageStartDate, stageEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
		}
	}
	err = a.ApplicationFacade.UpdateApplication(ctx, a.authSrvClientUserService, request.ApplicationId, request.Title, *startDate, *endDate, request.Scale, request.Description)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	for _, member := range request.Members {
		err := a.ApplicationMemberFacade.UpdateApplicationMember(member.Id, request.ApplicationId, member.MemberId, member.EventRoleId)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, budget := range request.Budgets {
		err := a.ApplicationBudgetFacade.UpdateApplicationBudget(budget.Id, request.ApplicationId, budget.Description, budget.UnitPrice, budget.Amount, budget.TotalPrice)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, stage := range request.Stages {
		stageStartDate, stageEndDate, err := validator.StartDateEndDateValidator(stage.StartDate, stage.EndDate, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		err = a.ApplicationStageFacade.UpdateApplicationStage(stage.Id, request.ApplicationId, stage.Name, stage.Location, *stageStartDate, *stageEndDate, stage.Description)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		for _, content := range stage.Contents {
			contentStartDate, contentEndDate, err := validator.StartDateEndDateValidator(content.StartDate, content.EndDate, stageStartDate, stageEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			err = a.ApplicationContentFacade.UpdateApplicationContent(content.Id, request.ApplicationId, content.Name, content.Description, content.LeaderId, *contentStartDate, *contentEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
		}
	}
	err = a.ApplicationApprovalFacade.AddApplicationApproval(ctx, a.authSrvClientUserService, a.authSrvClientUserAssignEventRoleService, a.authSrvClientRolePermission, a.eventSrvClientEvent, request.ApplicationId, 3)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Success = true
	return nil
}

func (a *ApplicationHandler) CancelApplication(ctx context.Context, request *application.CancelApplicationRequest, response *application.CancelApplicationResponse) error {
	err := a.ApplicationApprovalFacade.AddApplicationApproval(ctx, a.authSrvClientUserService, a.authSrvClientUserAssignEventRoleService, a.authSrvClientRolePermission, a.eventSrvClientEvent, request.ApplicationId, 6)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Success = true
	return nil
}

func (a *ApplicationHandler) GetApplicationsFromEvents(ctx context.Context, request *application.GetApplicationsFromEventsRequest, response *application.GetApplicationsFromEventsResponse) error {
	applications, err := a.ApplicationFacade.GetApplicationsByIds(request.Ids)
	log.Info(applications.Applications)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Applications = applications.Applications
	return nil
}

func (a *ApplicationHandler) GetApplicationsList(ctx context.Context, request *application.GetApplicationsListRequest, response *application.GetApplicationsListResponse) error {
	res, err := a.ApplicationFacade.GetApplicationList(ctx, a.authSrvClientUserService, a.authSrvClientRolePermission)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Applications = res.Applications
	return nil
}

func (a *ApplicationHandler) CreateApplication(ctx context.Context, request *application.CreateApplicationRequest, response *application.CreateApplicationResponse) error {
	startDate, endDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, nil, nil)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	err = validator.ApplicationValidator(&request.Title, &request.Description, &request.Scale)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	for _, member := range request.Members {
		err := validator.ApplicationMemberValidator(&member.MemberId, &member.EventRoleId)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, budget := range request.Budgets {
		err := validator.ApplicationBudgetValidator(&budget.Description, &budget.UnitPrice, &budget.Amount, &budget.TotalPrice)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, stage := range request.Stages {
		err := validator.ApplicationStageValidator(&stage.Name, &stage.Location, &stage.Description)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		stageStartDate, stageEndDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		for _, content := range stage.Contents {
			err := validator.ApplicationContentValidator(&content.Name, &content.Description, &content.LeaderId)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			_, _, err = validator.StartDateEndDateValidator(request.StartDate, request.EndDate, stageStartDate, stageEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
		}
	}
	application, err := a.ApplicationFacade.AddApplication(ctx, a.authSrvClientUserService, a.authSrvClientRolePermission, request.Title, *startDate, *endDate, request.Scale, request.Description)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	for _, member := range request.Members {
		err = a.ApplicationMemberFacade.AddApplicationMember(uint32(application.ID), member.MemberId, member.EventRoleId)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, budget := range request.Budgets {
		err = a.ApplicationBudgetFacade.AddApplicationBudget(uint32(application.ID), budget.Description, budget.UnitPrice, budget.Amount, budget.TotalPrice)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
	}
	for _, stage := range request.Stages {
		stageStartDate, stageEndDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, startDate, endDate)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		applicationStage, err := a.ApplicationStageFacade.AddApplicationStage(uint32(application.ID), stage.Name, stage.Location, *stageStartDate, *stageEndDate, stage.Description)
		if err != nil {
			return EmsErrorToHttpError(err)
		}
		for _, content := range stage.Contents {
			contentStartDate, contentEndDate, err := validator.StartDateEndDateValidator(request.StartDate, request.EndDate, stageStartDate, stageEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
			err = a.ApplicationContentFacade.AddApplicationContent(uint32(applicationStage.ID), content.Name, content.Description, content.LeaderId, *contentStartDate, *contentEndDate)
			if err != nil {
				return EmsErrorToHttpError(err)
			}
		}
	}
	err = a.ApplicationApprovalFacade.AddApplicationApproval(ctx, a.authSrvClientUserService, a.authSrvClientUserAssignEventRoleService, a.authSrvClientRolePermission, a.eventSrvClientEvent, uint32(application.ID), 1)
	if err != nil {
		return EmsErrorToHttpError(err)
	}
	response.Success = true
	return nil
}

