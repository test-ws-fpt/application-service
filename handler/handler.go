package handler

import (
	"ems/application-service/injector"
	emsError "ems/shared/error"
	application "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
	httpError "github.com/micro/go-micro/v2/errors"
	"github.com/micro/go-micro/v2/server"
)

func Apply1(server server.Server, injector injector.Injector, authSrvClientUserService authPB.UserService, authSrvClientRolePermission authPB.RolePermissionService, eventSrvClientEvent eventPB.EventService, authSrvClientUserAssignEventRoleService authPB.UserAssignEventRoleService) {
	application.RegisterApplicationHandler(server, NewApplicationHandler(injector.ApplicationFacade,
		injector.ApplicationApprovalFacade,
		injector.ApplicationMemberFacade,
		injector.ApplicationBudgetFacade,
		injector.ApplicationStageFacade,
		injector.ApplicationContentFacade,
		authSrvClientUserService,
		authSrvClientRolePermission,
		eventSrvClientEvent,
		authSrvClientUserAssignEventRoleService))
}

func EmsErrorToHttpError (err error) error {
	re, ok := err.(*emsError.ResultError)
	if ok {
		if re.ErrorCode == 400 {
			return httpError.BadRequest("go.micro.service.application", re.Err.Error())
		} else if re.ErrorCode == 401 {
			return httpError.Unauthorized("go.micro.service.application", re.Err.Error())
		} else if re.ErrorCode == 403 {
			return httpError.Forbidden("go.micro.service.application", re.Err.Error())
		} else if re.ErrorCode == 404 {
			return httpError.NotFound("go.micro.service.application", re.Err.Error())
		} else {
			return httpError.InternalServerError("go.micro.service.application", re.Err.Error())
		}
	} else {
		return httpError.InternalServerError("go.micro.service.application", "unknown server error")
	}
}