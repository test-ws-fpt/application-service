package enum


// convert application status code from int to string, output depends on the user's permission
func StatusCodeToString(statusCode, permissionId uint) string {
	switch permissionId {
		case 3:
			switch statusCode {
			case 1:
				return "Pending For Preliminary Approval"
			case 2:
				return "Waiting For Final Update"
			case 3:
				return "Pending For Final Approval"
			case 4:
				return "Approved"
			case 5:
				return "Rejected"
			case 6:
				return "Canceled"
			default:
				return "Unknown"
			}
		case 2:
			switch statusCode {
			case 1:
				return "Pending"
			case 2:
				return "Approved"
			case 5:
				return "Rejected"
			default:
				return "Unknown"
			}
		case 1:
			switch statusCode {
			case 3:
				return "Pending"
			case 4:
				return "Approved"
			case 5:
				return "Rejected"
			default:
				return "Unknown"
			}
		default:
			return "Unknown"
	}
}
