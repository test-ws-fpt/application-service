package model

type ApplicationBudget struct {
	ID     			uint `gorm:"primary_key;auto_increment"`
	ApplicationId 	uint
	Description		string
	Application		Application
	UnitPrice		float32
	Amount			uint
	TotalPrice		float32
}

func (ApplicationBudget) TableName() string {
	return "application_budget"
}
