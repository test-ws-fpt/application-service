package model

import (
	"time"
)

type ApplicationStage struct {
	ID     				uint `gorm:"primary_key;auto_increment"`
	ApplicationId 		uint
	Application			Application
	Name				string
	Location			string
	StartDate			time.Time
	EndDate				time.Time
	Description			string
	ApplicationContents []ApplicationContent
}

func (ApplicationStage) TableName() string {
	return "application_stage"
}

