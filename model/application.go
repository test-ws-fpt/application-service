package model

import (
	"time"
)

type Application struct {
	ID     					uint `gorm:"primary_key;auto_increment"`
	Title 					string
	StartDate				time.Time
	EndDate 				time.Time
	Scale 					uint
	Description				string
	CreatedBy				uint
	OrganizationId 			uint
	ApplicationApprovals 	[]ApplicationApproval
	ApplicationBudgets 		[]ApplicationBudget
	ApplicationMembers 		[]ApplicationMember
	ApplicationStages 		[]ApplicationStage
}

func (Application) TableName() string {
	return "application"
}
