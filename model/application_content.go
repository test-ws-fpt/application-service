package model

import (
	"time"
)

type ApplicationContent struct {
	ID     				uint `gorm:"primary_key;auto_increment"`
	ApplicationStageId 	uint
	ApplicationStage	ApplicationStage
	Name				string
	Description			string
	LeaderId			uint
	StartDate			time.Time
	EndDate				time.Time
}

func (ApplicationContent) TableName() string {
	return "application_content"
}
