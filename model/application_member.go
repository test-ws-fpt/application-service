package model



type ApplicationMember struct {
	ID     			uint `gorm:"primary_key;auto_increment"`
	ApplicationId 	uint
	Application		Application
	MemberId 		uint
	EventRoleId		uint
}

func (ApplicationMember) TableName() string {
	return "application_member"
}

