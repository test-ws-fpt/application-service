package model

import (
	"time"
)

type ApplicationApproval struct {
	ID     			uint `gorm:"primary_key;auto_increment"`
	ApplicationId 	uint
	CreatedAt		time.Time
	Application		Application
	StatusCode 		uint
	ApproverId		uint
	IsActive		bool
}

func (ApplicationApproval) TableName() string {
	return "application_approval"
}
