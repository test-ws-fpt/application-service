package populator

import (
	"ems/application-service/enum"
	"ems/application-service/model"
	applicationPB "ems/shared/proto/application"
)

//convert array of application model to message protobuf
func GetApplicationListPopulator(applicationApprovals *[]model.ApplicationApproval, permissionId uint) *applicationPB.GetApplicationsListResponse {
	var res applicationPB.GetApplicationsListResponse
		for _, applicationApproval := range *applicationApprovals {
			res.Applications = append(res.Applications, &applicationPB.ApplicationDict{
				Id:     uint32(applicationApproval.Application.ID),
				Title:  applicationApproval.Application.Title,
				Status: enum.StatusCodeToString(applicationApproval.StatusCode, permissionId),
			})
		}
	return &res
}
