package populator

import (
	"ems/application-service/enum"
	"ems/application-service/model"
	applicationPB "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
)

//convert application model, member model, budget model, approval model, stage model, content model to message protobuf
func GetApplicationByIdPopulator(application *model.Application, members *[]model.ApplicationMember, budgets *[]model.ApplicationBudget, stages *[]model.ApplicationStage, approval *model.ApplicationApproval, createdBy, approver string, eventRunners *authPB.GetEventRunnersResponse) *applicationPB.ViewApplicationDetailsResponse {
	res := applicationPB.ViewApplicationDetailsResponse{
		Title:       application.Title,
		StartDate:   application.StartDate.Format("02-01-2006"),
		EndDate:     application.EndDate.Format("02-01-2006"),
		Scale:       uint32(application.Scale),
		Description: application.Description,
		Id:          uint32(application.ID),
		CreatedBy:   createdBy,
		Status:      enum.StatusCodeToString(approval.StatusCode, 3),
	}
	if approver != "" {
		res.Approver = approver
	}
	for _, member := range *members {
		for _, e := range eventRunners.EventRunners {
			if *e.Id == uint32(member.MemberId) {
				res.Members = append(res.Members, &applicationPB.ViewApplicationMemberDict{
					MemberId:    uint32(member.MemberId),
					EventRoleId: uint32(member.EventRoleId),
					Id:          uint32(member.ID),
					Name:		 *e.Name,
				})
			}
		}
	}
	for _, budget := range *budgets {
		res.Budgets = append(res.Budgets, &applicationPB.UpdateApplicationBudgetDict{
			Description: budget.Description,
			UnitPrice:   budget.UnitPrice,
			Amount:      uint32(budget.Amount),
			TotalPrice:  budget.TotalPrice,
			Id:          uint32(budget.ID),
		})
	}
	for _, stage := range *stages {
		resStage := applicationPB.ViewApplicationStageDict{
			Name:        stage.Name,
			Location:    stage.Location,
			StartDate:   stage.StartDate.Format("02-01-2006"),
			EndDate:     stage.EndDate.Format("02-01-2006"),
			Description: stage.Description,
			Id:          uint32(stage.ID),
		}
		for _, content := range stage.ApplicationContents {
			c := applicationPB.ViewApplicationContentDict{
				Name:        content.Name,
				Description: content.Description,
				LeaderId:    uint32(content.LeaderId),
				StartDate:   content.StartDate.Format("02-01-2006"),
				EndDate:     content.EndDate.Format("02-01-2006"),
				Id:          uint32(content.ID),
			}
			for _, e := range eventRunners.EventRunners {
				if *e.Id == uint32(content.LeaderId) {
					c.LeaderName = *e.Name
				}
			}
			resStage.Contents = append(resStage.Contents, &c)
		}
		res.Stages = append(res.Stages, &resStage)
	}
	return &res
}
