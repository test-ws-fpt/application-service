package repository

import "ems/application-service/model"

type IApplicationRepository interface {
	GetApplicationById(Id uint) (*model.Application, error)
	GetApplicationsByIds(Ids []uint) (*[]model.Application, error)
	AddApplication(application *model.Application) (*model.Application, error)
	UpdateApplication(application *model.Application) error
}

type IApplicationBudgetRepository interface {
	AddApplicationBudget(applicationBudget *model.ApplicationBudget) error
	UpdateApplicationBudget(applicationBudget *model.ApplicationBudget) error
	GetApplicationBudgets(applicationId uint) (*[]model.ApplicationBudget, error)
}

type IApplicationMemberRepository interface {
	AddApplicationMember(applicationMember *model.ApplicationMember) error
	UpdateApplicationMember(applicationMember *model.ApplicationMember) error
	GetApplicationMembers(applicationId uint) (*[]model.ApplicationMember, error)
}

type IApplicationStageRepository interface {
	AddApplicationStage(applicationStage *model.ApplicationStage) (*model.ApplicationStage, error)
	UpdateApplicationStage(applicationStage *model.ApplicationStage) error
	GetApplicationStages(applicationId uint) (*[]model.ApplicationStage, error)
}

type IApplicationContentRepository interface {
	AddApplicationContent(applicationContent *model.ApplicationContent) error
	UpdateApplicationContent(applicationContent *model.ApplicationContent) error
	GetApplicationContents(applicationId uint) (*[]model.ApplicationContent, error)
}

type IApplicationApprovalRepository interface {
	GetApplicationApproval(applicationId uint) (*model.ApplicationApproval, error)
	AddApplicationApproval(applicationApproval *model.ApplicationApproval) error
	GetApplicationList(userId, organizationId, permissionId uint) (*[]model.ApplicationApproval, error)
	DeactivateApplicationApproval(applicationId uint) error
}