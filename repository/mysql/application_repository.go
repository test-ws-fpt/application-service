package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationRepository = (*ApplicationRepository)(nil)

var ApplicationRepositorySet = wire.NewSet(wire.Struct(new(ApplicationRepository), "*"), wire.Bind(new(repo.IApplicationRepository), new(*ApplicationRepository)))

type ApplicationRepository struct {
	DB *gorm.DB
}


func (e *ApplicationRepository) UpdateApplication(application *model.Application) error {
	if res := e.DB.Omit("created_by", "organization_id").Save(&application); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when updating application")}
	}
	return nil
}

func (e *ApplicationRepository) AddApplication(application *model.Application) (*model.Application, error) {
	if res := e.DB.Create(&application); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding application")}
	}
	return application, nil
}

func (e *ApplicationRepository) GetApplicationById(Id uint) (*model.Application, error) {
	Application := model.Application{}
	if res := e.DB.Where("id = ?", Id).First(&Application);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("application does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting application")}
		}
	}
	return &Application, nil
}

func (e *ApplicationRepository) GetApplicationsByIds(Ids []uint) (*[]model.Application, error) {
	var Applications []model.Application
	if res := e.DB.Where("id in (?)", Ids).Find(&Applications);  res.Error != nil {
		log.Info(res.Error.Error())
		if gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 400, Err: errors.New("application does not exist")}
		} else {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting applications")}
		}
	}
	return &Applications, nil
}


