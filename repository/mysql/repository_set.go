package mysql

import "github.com/google/wire"

// RepositorySet
var RepositorySet = wire.NewSet(
	ApplicationRepositorySet,
	ApplicationContentRepositorySet,
	ApplicationStageRepositorySet,
	ApplicationMemberRepositorySet,
	ApplicationBudgetRepositorySet,
	ApplicationApprovalRepositorySet,
)
