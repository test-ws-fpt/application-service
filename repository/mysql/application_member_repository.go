package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationMemberRepository = (*ApplicationMemberRepository)(nil)

var ApplicationMemberRepositorySet = wire.NewSet(wire.Struct(new(ApplicationMemberRepository), "*"), wire.Bind(new(repo.IApplicationMemberRepository), new(*ApplicationMemberRepository)))

type ApplicationMemberRepository struct {
	DB *gorm.DB
}

func (a *ApplicationMemberRepository) AddApplicationMember(applicationMember *model.ApplicationMember) error {
	if res := a.DB.Create(&applicationMember); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when adding application member"),
		}
	}
	return nil
}

func (a *ApplicationMemberRepository) UpdateApplicationMember(applicationMember *model.ApplicationMember) error {
	if res := a.DB.Omit("application_id").Save(&applicationMember); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when updating application member"),
		}
	}
	return nil
}

func (a *ApplicationMemberRepository) GetApplicationMembers(applicationId uint) (*[]model.ApplicationMember, error) {
	var applicationMembers []model.ApplicationMember
	if res := a.DB.Where("application_id = ?", applicationId).Find(&applicationMembers); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{
				ErrorCode: 500,
				Err:       errors.New("error when finding application members"),
			}
		}
	}
	return &applicationMembers, nil
}

