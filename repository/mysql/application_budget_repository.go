package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationBudgetRepository = (*ApplicationBudgetRepository)(nil)

var ApplicationBudgetRepositorySet = wire.NewSet(wire.Struct(new(ApplicationBudgetRepository), "*"), wire.Bind(new(repo.IApplicationBudgetRepository), new(*ApplicationBudgetRepository)))

type ApplicationBudgetRepository struct {
	DB *gorm.DB
}

func (a *ApplicationBudgetRepository) AddApplicationBudget(applicationBudget *model.ApplicationBudget) error {
	if res := a.DB.Create(&applicationBudget); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when adding application budget"),
		}
	}
	return nil
}

func (a *ApplicationBudgetRepository) UpdateApplicationBudget(applicationBudget *model.ApplicationBudget) error {
	if res := a.DB.Omit("application_id").Save(&applicationBudget); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when updating application budget"),
		}
	}
	return nil
}

func (a *ApplicationBudgetRepository) GetApplicationBudgets(applicationId uint) (*[]model.ApplicationBudget, error) {
	var applicationBudgets []model.ApplicationBudget
	if res := a.DB.Where("application_id = ?", applicationId).Find(&applicationBudgets); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{
				ErrorCode: 500,
				Err:       errors.New("error when finding application budgets"),
			}
		}
	}
	return &applicationBudgets, nil
}

