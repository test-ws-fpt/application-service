package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationContentRepository = (*ApplicationContentRepository)(nil)

var ApplicationContentRepositorySet = wire.NewSet(wire.Struct(new(ApplicationContentRepository), "*"), wire.Bind(new(repo.IApplicationContentRepository), new(*ApplicationContentRepository)))

type ApplicationContentRepository struct {
	DB *gorm.DB
}

func (a *ApplicationContentRepository) AddApplicationContent(applicationContent *model.ApplicationContent) error {
	if res := a.DB.Create(&applicationContent); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when adding application content"),
		}
	}
	return nil
}

func (a *ApplicationContentRepository) UpdateApplicationContent(applicationContent *model.ApplicationContent) error {
	if res := a.DB.Omit("application_id").Save(&applicationContent); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when updating application content"),
		}
	}
	return nil
}

func (a *ApplicationContentRepository) GetApplicationContents(applicationId uint) (*[]model.ApplicationContent, error) {
	var applicationContents []model.ApplicationContent
	if res := a.DB.Where("application_id = ?", applicationId).Find(&applicationContents); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{
				ErrorCode: 500,
				Err:       errors.New("error when finding application contents"),
			}
		}
	}
	return &applicationContents, nil
}

