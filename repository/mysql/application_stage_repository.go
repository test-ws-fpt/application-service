package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationStageRepository = (*ApplicationStageRepository)(nil)

var ApplicationStageRepositorySet = wire.NewSet(wire.Struct(new(ApplicationStageRepository), "*"), wire.Bind(new(repo.IApplicationStageRepository), new(*ApplicationStageRepository)))

type ApplicationStageRepository struct {
	DB *gorm.DB
}

func (a *ApplicationStageRepository) AddApplicationStage(applicationStage *model.ApplicationStage) (*model.ApplicationStage, error) {
	if res := a.DB.Create(&applicationStage); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when adding application stage"),
		}
	}
	return applicationStage, nil
}

func (a *ApplicationStageRepository) UpdateApplicationStage(applicationStage *model.ApplicationStage) error {
	if res := a.DB.Omit("application_id").Save(&applicationStage); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when updating application stage"),
		}
	}
	return nil
}

func (a *ApplicationStageRepository) GetApplicationStages(applicationId uint) (*[]model.ApplicationStage, error) {
	var applicationStages []model.ApplicationStage
	if res := a.DB.Preload("ApplicationContents").Where("application_id = ?", applicationId).Find(&applicationStages); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{
				ErrorCode: 500,
				Err:       errors.New("error when finding application stages"),
			}
		}
	}
	return &applicationStages, nil
}

