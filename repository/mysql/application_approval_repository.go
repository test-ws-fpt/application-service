package mysql

import (
	"ems/application-service/model"
	repo "ems/application-service/repository"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	log "github.com/micro/go-micro/v2/logger"
)

var _ repo.IApplicationApprovalRepository = (*ApplicationApprovalRepository)(nil)

var ApplicationApprovalRepositorySet = wire.NewSet(wire.Struct(new(ApplicationApprovalRepository), "*"), wire.Bind(new(repo.IApplicationApprovalRepository), new(*ApplicationApprovalRepository)))

type ApplicationApprovalRepository struct {
	DB *gorm.DB
}

func (a *ApplicationApprovalRepository) DeactivateApplicationApproval(applicationId uint) error {
	var applicationApproval model.ApplicationApproval
	if res := a.DB.Where("application_id = ? AND is_active = ?", applicationId, 1).First(&applicationApproval); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting application approval")}
	}
	applicationApproval.IsActive = false
	if res := a.DB.Save(&applicationApproval); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when changing application approval")}
	}
	return nil
}

func (a *ApplicationApprovalRepository) GetApplicationList(userId, organizationId, permissionId uint) (*[]model.ApplicationApproval, error) {
	var applicationApprovals []model.ApplicationApproval
	var applicationApproval model.ApplicationApproval
	res := a.DB.Model(&applicationApproval)
	if permissionId == 1 {
		res = res.Preload("Application", "organization_id = ?", organizationId).Where("status_code in (3,4,5) AND is_active = 1")
	} else if permissionId == 2 {
		res = res.Preload("Application", "organization_id = ?", organizationId).Where("(status_code = 1 AND is_active = 1) OR (approver_id = ?)", userId)
	} else {
		res = res.Preload("Application", "organization_id = ? AND created_by = ?", organizationId, userId).Where("is_active = 1")
	}
	if res = res.Order("status_code asc").Find(&applicationApprovals); res.Error != nil {
		if !gorm.IsRecordNotFoundError(res.Error) {
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting applications")}
		}
	}
	return &applicationApprovals, nil
}

func (a *ApplicationApprovalRepository) GetApplicationApproval(applicationId uint) (*model.ApplicationApproval, error) {
	var applicationApproval model.ApplicationApproval
	if res := a.DB.Where("application_id = ?", applicationId).Where("is_active = 1").First(&applicationApproval); res.Error != nil {
		log.Info(res.Error.Error())
		return nil, &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when finding application approval"),
		}
	}
	return &applicationApproval, nil
}

func (a *ApplicationApprovalRepository) AddApplicationApproval(applicationApproval *model.ApplicationApproval) error {
	if res := a.DB.Create(&applicationApproval); res.Error != nil {
		log.Info(res.Error.Error())
		return &emsError.ResultError{
			ErrorCode: 500,
			Err:       errors.New("error when adding application approval"),
		}
	}
	return nil
}
