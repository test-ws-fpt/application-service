package main

import (
	"ems/application-service/handler"
	"ems/application-service/injector"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
	"github.com/micro/go-micro/v2"
	log "github.com/micro/go-micro/v2/logger"
)

func main() {
	// New Service
	service := micro.NewService(
		micro.Name("go.micro.service.application"),
		micro.Version("latest"),
	)

	// Initialise service
	service.Init()
	injector, _, _ := injector.BuildInjector()
	authSrvClientUser := authPB.NewUserService("go.micro.service.auth", service.Client())
	authSrvClientRolePermission := authPB.NewRolePermissionService("go.micro.service.auth", service.Client())
	eventSrvClientEvent := eventPB.NewEventService("go.micro.service.event", service.Client())
	authSrvClientUserAssignEventRoleService := authPB.NewUserAssignEventRoleService("go.micro.service.auth", service.Client())
	handler.Apply1(service.Server(), *injector, authSrvClientUser, authSrvClientRolePermission, eventSrvClientEvent, authSrvClientUserAssignEventRoleService)
	// Register Handler


	// Run service
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
