package service

import (
	"ems/application-service/model"
	"time"
)

type IApplicationService interface {
	// call GetApplicationsByIds function from Repository layer
	GetApplicationsByIds(Ids []uint32) (*[]model.Application, error)

	// define application model, then call AddApplication function from Repository layer
	AddApplication(title string, startDate, endDate time.Time, scale uint32, description string, createdBy,
		organizationId uint32) (*model.Application, error)

	// define application model, then call UpdateApplication function from Repository layer
	UpdateApplication(id uint32, title string, startDate, endDate time.Time, scale uint32, description string,
		createdBy, organizationId uint32) error

	// call GetApplicationById function from Repository layer
	GetApplicationById(Id uint32) (*model.Application, error)
}

type IApplicationBudgetService interface {
	// define application budget model, then call AddApplicationBudget function from Repository layer
	AddApplicationBudget(applicationId uint32, description string, unitPrice float32,
		amount uint32, totalPrice float32) error

	// define application budget model, then call UpdateApplicationBudget function from Repository layer
	UpdateApplicationBudget(id, applicationId uint32, description string, unitPrice float32,
		amount uint32, totalPrice float32) error

	// call GetApplicationBudgets function from Repository layer
	GetApplicationBudgets(applicationId uint32) (*[]model.ApplicationBudget, error)
}

type IApplicationMemberService interface {
	// define application member model, then call AddApplicationMember function from Repository layer
	AddApplicationMember(applicationId, memberId, eventRoleId uint32) error

	// define application member model, then call UpdateApplicationMember function from Repository layer
	UpdateApplicationMember(id, applicationId, memberId, eventRoleId uint32) error

	// call GetApplicationMembers function from Repository layer
	GetApplicationMembers(applicationId uint32) (*[]model.ApplicationMember, error)
}

type IApplicationStageService interface {
	AddApplicationStage(applicationId uint32, name, location string, startDate, endDate time.Time,
		description string) (*model.ApplicationStage, error)
	UpdateApplicationStage(id, applicationId uint32, name, location string, startDate, endDate time.Time,
		description string) error
	GetApplicationStages(applicationId uint32) (*[]model.ApplicationStage, error)
}

type IApplicationContentService interface {
	AddApplicationContent(applicationStageId uint32, name, description string, leaderId uint32,
		startDate, endDate time.Time) error
	UpdateApplicationContent(id, applicationStageId uint32, name, description string, leaderId uint32,
		startDate, endDate time.Time) error
	GetApplicationContents(applicationId uint32) (*[]model.ApplicationContent, error)
}

type IApplicationApprovalService interface {
	GetApplicationApproval(applicationId uint32) (*model.ApplicationApproval, error)
	AddApplicationApproval(applicationId, status, approverId uint32) error
	GetApplicationList(userId, organizationId, permissionId uint32) (*[]model.ApplicationApproval, error)
}