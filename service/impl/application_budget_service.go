package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	"github.com/google/wire"
)

var _ service.IApplicationBudgetService = (*ApplicationBudgetService)(nil)

var ApplicationBudgetServiceSet = wire.NewSet(wire.Struct(new(ApplicationBudgetService), "*"), wire.Bind(new(service.IApplicationBudgetService), new(*ApplicationBudgetService)))

type ApplicationBudgetService struct {
	ApplicationBudgetRepository repository.IApplicationBudgetRepository
}

func (a *ApplicationBudgetService) AddApplicationBudget(applicationId uint32, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	applicationBudget := model.ApplicationBudget{
		ApplicationId: uint(applicationId),
		Description:   description,
		UnitPrice:     unitPrice,
		Amount:        uint(amount),
		TotalPrice:    totalPrice,
	}
	return a.ApplicationBudgetRepository.AddApplicationBudget(&applicationBudget)
}

func (a *ApplicationBudgetService) UpdateApplicationBudget(id, applicationId uint32, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	applicationBudget := model.ApplicationBudget{
		ID:            uint(id),
		ApplicationId: uint(applicationId),
		Description:   description,
		UnitPrice:     unitPrice,
		Amount:        uint(amount),
		TotalPrice:    totalPrice,
	}
	return a.ApplicationBudgetRepository.UpdateApplicationBudget(&applicationBudget)
}

func (a *ApplicationBudgetService) GetApplicationBudgets(applicationId uint32) (*[]model.ApplicationBudget, error) {
	return a.ApplicationBudgetRepository.GetApplicationBudgets(uint(applicationId))
}
