package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"time"
)

var _ service.IApplicationApprovalService = (*ApplicationApprovalService)(nil)

var ApplicationApprovalServiceSet = wire.NewSet(wire.Struct(new(ApplicationApprovalService), "*"), wire.Bind(new(service.IApplicationApprovalService), new(*ApplicationApprovalService)))

type ApplicationApprovalService struct {
	ApplicationApprovalRepository repository.IApplicationApprovalRepository
	ApplicationRepository repository.IApplicationRepository
}

func (a *ApplicationApprovalService) GetApplicationApproval(applicationId uint32) (*model.ApplicationApproval, error) {
	return a.ApplicationApprovalRepository.GetApplicationApproval(uint(applicationId))
}

func (a *ApplicationApprovalService) AddApplicationApproval(applicationId, status, approverId uint32) error {
	if status > 6 || status < 1 {
		return &emsError.ResultError{
			ErrorCode: 403,
			Err:       errors.New("invalid status of application"),
		}
	} else if status > 1 && status <= 6 {
		oldApplicationApproval, err := a.ApplicationApprovalRepository.GetApplicationApproval(uint(applicationId))
		if err != nil {
			return err
		}
		application, err := a.ApplicationRepository.GetApplicationById(uint(applicationId))
		if err != nil {
			return err
		}
		if oldApplicationApproval.StatusCode >= 4 {
			return &emsError.ResultError{
				ErrorCode: 403,
				Err:       errors.New("cannot change status of application"),
			}
		}
		switch status {
			case 6:
				if oldApplicationApproval.StatusCode != 1 {
					return &emsError.ResultError{
						ErrorCode: 403,
						Err:       errors.New("cannot cancel this application"),
					}
				}
				if application.CreatedBy != uint(approverId) {
					return &emsError.ResultError{
						ErrorCode: 403,
						Err:       errors.New("this user is not permitted to cancel this application"),
					}
				}
			case 5:
				if oldApplicationApproval.StatusCode == 2 {
					return &emsError.ResultError{
						ErrorCode: 403,
						Err:       errors.New("cannot reject this application"),
					}
				}
			case 3:
				if application.CreatedBy != uint(approverId) {
					return &emsError.ResultError{
						ErrorCode: 403,
						Err:       errors.New("this user is not permitted to update this application"),
					}
				}
			default:
				if uint(status) != oldApplicationApproval.StatusCode + 1 {
					return &emsError.ResultError{
						ErrorCode: 403,
						Err:       errors.New("invalid status of application"),
					}
				}
		}
		err = a.ApplicationApprovalRepository.DeactivateApplicationApproval(uint(applicationId))
		if err != nil {
			return err
		}
		applicationApproval := model.ApplicationApproval{
			ApplicationId: uint(applicationId),
			CreatedAt:     time.Now(),
			StatusCode:    uint(status),
			ApproverId:    uint(approverId),
			IsActive: true,
		}
		err = a.ApplicationApprovalRepository.AddApplicationApproval(&applicationApproval)
		if err != nil {
			return err
		}
		if status == 2 {
			err = a.ApplicationApprovalRepository.DeactivateApplicationApproval(uint(applicationId))
			if err != nil {
				return err
			}
			applicationApproval := model.ApplicationApproval{
				ApplicationId: uint(applicationId),
				CreatedAt:     time.Now(),
				StatusCode:    3,
				IsActive: true,
			}
			return a.ApplicationApprovalRepository.AddApplicationApproval(&applicationApproval)
		} else {
			return nil
		}
	} else {
		oldApplicationApproval, _ := a.ApplicationApprovalRepository.GetApplicationApproval(uint(applicationId))
		if oldApplicationApproval != nil {
			return &emsError.ResultError{
				ErrorCode: 403,
				Err:       errors.New("invalid status of application"),
			}
		}
		applicationApproval := model.ApplicationApproval{
			ApplicationId: uint(applicationId),
			CreatedAt:     time.Now(),
			StatusCode:    uint(status),
			IsActive: true,
		}
		return a.ApplicationApprovalRepository.AddApplicationApproval(&applicationApproval)
	}
}



func (a *ApplicationApprovalService) GetApplicationList(userId, organizationId, permissionId uint32) (*[]model.ApplicationApproval, error) {
	return a.ApplicationApprovalRepository.GetApplicationList(uint(userId), uint(organizationId), uint(permissionId))
}