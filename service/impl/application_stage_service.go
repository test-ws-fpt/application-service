package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	"github.com/google/wire"
	"time"
)

var _ service.IApplicationStageService = (*ApplicationStageService)(nil)

var ApplicationStageServiceSet = wire.NewSet(wire.Struct(new(ApplicationStageService), "*"), wire.Bind(new(service.IApplicationStageService), new(*ApplicationStageService)))

type ApplicationStageService struct {
	ApplicationStageRepository repository.IApplicationStageRepository
}

func (a *ApplicationStageService) AddApplicationStage(applicationId uint32, name, location string, startDate, endDate time.Time, description string) (*model.ApplicationStage, error) {
	applicationStage := model.ApplicationStage{
		ApplicationId:       uint(applicationId),
		Name:                name,
		Location:            location,
		StartDate:           startDate,
		EndDate:             endDate,
		Description:         description,
	}
	return a.ApplicationStageRepository.AddApplicationStage(&applicationStage)
}

func (a *ApplicationStageService) UpdateApplicationStage(id, applicationId uint32, name, location string, startDate, endDate time.Time, description string) error {
	applicationStage := model.ApplicationStage{
		ID: uint(id),
		ApplicationId:       uint(applicationId),
		Name:                name,
		Location:            location,
		StartDate:           startDate,
		EndDate:             endDate,
		Description:         description,
	}
	return a.ApplicationStageRepository.UpdateApplicationStage(&applicationStage)
}

func (a *ApplicationStageService) GetApplicationStages(applicationId uint32) (*[]model.ApplicationStage, error) {
	return a.ApplicationStageRepository.GetApplicationStages(uint(applicationId))
}



