package impl

import "github.com/google/wire"

var ServiceSet = wire.NewSet(
	ApplicationServiceSet,
	ApplicationBudgetServiceSet,
	ApplicationContentServiceSet,
	ApplicationMemberServiceSet,
	ApplicationStageServiceSet,
	ApplicationApprovalServiceSet,
)
