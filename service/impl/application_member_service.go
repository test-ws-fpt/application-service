package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	"github.com/google/wire"
)

var _ service.IApplicationMemberService = (*ApplicationMemberService)(nil)

var ApplicationMemberServiceSet = wire.NewSet(wire.Struct(new(ApplicationMemberService), "*"), wire.Bind(new(service.IApplicationMemberService), new(*ApplicationMemberService)))

type ApplicationMemberService struct {
	ApplicationMemberRepository repository.IApplicationMemberRepository
}

func (a *ApplicationMemberService) AddApplicationMember(applicationId, memberId, eventRoleId uint32) error {
	applicationMember := model.ApplicationMember{
		ApplicationId: uint(applicationId),
		MemberId:      uint(memberId),
		EventRoleId:   uint(eventRoleId),
	}
	return a.ApplicationMemberRepository.AddApplicationMember(&applicationMember)
}

func (a *ApplicationMemberService) UpdateApplicationMember(id, applicationId, memberId, eventRoleId uint32) error {
	applicationMember := model.ApplicationMember{
		ID: uint(id),
		ApplicationId: uint(applicationId),
		MemberId:      uint(memberId),
		EventRoleId:   uint(eventRoleId),
	}
	return a.ApplicationMemberRepository.UpdateApplicationMember(&applicationMember)
}

func (a *ApplicationMemberService) GetApplicationMembers(applicationId uint32) (*[]model.ApplicationMember, error) {
	return a.ApplicationMemberRepository.GetApplicationMembers(uint(applicationId))
}


