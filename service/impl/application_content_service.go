package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	"github.com/google/wire"
	"time"
)

var _ service.IApplicationContentService = (*ApplicationContentService)(nil)

var ApplicationContentServiceSet = wire.NewSet(wire.Struct(new(ApplicationContentService), "*"), wire.Bind(new(service.IApplicationContentService), new(*ApplicationContentService)))

type ApplicationContentService struct {
	ApplicationContentRepository repository.IApplicationContentRepository
}

func (a *ApplicationContentService) AddApplicationContent(applicationStageId uint32, name, description string, leaderId uint32, startDate, endDate time.Time) error {
	applicationContent := model.ApplicationContent{
		ApplicationStageId: uint(applicationStageId),
		Name:               name,
		Description:        description,
		LeaderId:           uint(leaderId),
		StartDate:          startDate,
		EndDate:            endDate,
	}
	return a.ApplicationContentRepository.AddApplicationContent(&applicationContent)
}

func (a *ApplicationContentService) UpdateApplicationContent(id, applicationStageId uint32, name, description string, leaderId uint32, startDate, endDate time.Time) error {
	applicationContent := model.ApplicationContent{
		ID: uint(id),
		ApplicationStageId: uint(applicationStageId),
		Name:               name,
		Description:        description,
		LeaderId:           uint(leaderId),
		StartDate:          startDate,
		EndDate:            endDate,
	}
	return a.ApplicationContentRepository.UpdateApplicationContent(&applicationContent)
}

func (a *ApplicationContentService) GetApplicationContents(applicationId uint32) (*[]model.ApplicationContent, error) {
	return a.ApplicationContentRepository.GetApplicationContents(uint(applicationId))
}



