package impl

import (
	"ems/application-service/model"
	"ems/application-service/repository"
	"ems/application-service/service"
	emsError "ems/shared/error"
	"errors"
	"github.com/google/wire"
	"time"
)

var _ service.IApplicationService = (*ApplicationService)(nil)

var ApplicationServiceSet = wire.NewSet(wire.Struct(new(ApplicationService), "*"), wire.Bind(new(service.IApplicationService), new(*ApplicationService)))

type ApplicationService struct {
	ApplicationRepository repository.IApplicationRepository
	ApplicationApprovalRepository repository.IApplicationApprovalRepository
}


func (a *ApplicationService) GetApplicationById(Id uint32) (*model.Application, error) {
	return a.ApplicationRepository.GetApplicationById(uint(Id))
}

func (a *ApplicationService) AddApplication(title string, startDate, endDate time.Time, scale uint32, description string, createdBy, organizationId uint32) (*model.Application, error) {
	application := model.Application{
		Title:                title,
		StartDate:            startDate,
		EndDate:              endDate,
		Scale:                uint(scale),
		Description:          description,
		CreatedBy:            uint(createdBy),
		OrganizationId:       uint(organizationId),
	}
	return a.ApplicationRepository.AddApplication(&application)
}

func (a *ApplicationService) UpdateApplication(id uint32, title string, startDate, endDate time.Time, scale uint32, description string, createdBy, organizationId uint32) error {
	oldApplication, err := a.ApplicationApprovalRepository.GetApplicationApproval(uint(id))
	if err != nil {
		return err
	}
	if oldApplication.StatusCode != 2 {
		return &emsError.ResultError{
			ErrorCode: 403,
			Err:       errors.New("this application is not allowed to be updated"),
		}
	}
	old, err := a.ApplicationRepository.GetApplicationById(uint(id))
	if err != nil {
		return err
	}
	if old.CreatedBy != uint(createdBy) {
		return &emsError.ResultError{
			ErrorCode: 403,
			Err:       errors.New("this user is not permitted to update this application"),
		}
	}
	application := model.Application{
		ID:                   uint(id),
		Title:                title,
		StartDate:            startDate,
		EndDate:              endDate,
		Scale:                uint(scale),
		Description:          description,
		CreatedBy:            uint(createdBy),
		OrganizationId:       uint(organizationId),
	}
	return a.ApplicationRepository.UpdateApplication(&application)
}

func (a *ApplicationService) GetApplicationsByIds(Ids []uint32) (*[]model.Application, error) {
	var convertedIds []uint
	for _, s := range Ids {
		convertedId := uint(s)
		convertedIds = append(convertedIds, convertedId)
	}
	return a.ApplicationRepository.GetApplicationsByIds(convertedIds)
}


