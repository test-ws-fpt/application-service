package validator

import (
	emsError "ems/shared/error"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
)

func ApplicationContentValidator(name, description *string, leaderId *uint32) error {
	if name == nil || *name == "" {
		log.Info("invalid content name")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("content name cannot be null"),
		}
	}
	if description == nil || *description == "" {
		log.Info("invalid content description")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("content description cannot be null"),
		}
	}
	if leaderId == nil {
		log.Info("invalid content leader")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("content leader id cannot be null"),
		}
	}
	return nil
}
