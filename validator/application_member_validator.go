package validator

import (
	emsError "ems/shared/error"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
)

func ApplicationMemberValidator(memberId, eventRoleId *uint32) error {
	if memberId == nil {
		log.Info("invalid member id")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("member id cannot be null"),
		}
	}
	if eventRoleId == nil {
		log.Info("invalid member event role")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("event role id cannot be null"),
		}
	}
	return nil
}
