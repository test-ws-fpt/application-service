package validator

import (
	emsError "ems/shared/error"
	"errors"
	"fmt"
	log "github.com/micro/go-micro/v2/logger"
	"time"
)

func StartDateEndDateValidator(startDateStr, endDateStr string, durationStartDate, durationEndDate *time.Time) (*time.Time, *time.Time, error) {
	startDate, err := time.Parse("02-01-2006", startDateStr)
	if err != nil {
		log.Info("invalid start date")
		return nil, nil, &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("invalid start date"),
		}
	}
	endDate, err := time.Parse("02-01-2006", endDateStr)
	if err != nil {
		log.Info("invalid end date")
		return nil, nil, &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("invalid end date"),
		}
	}
	if durationStartDate != nil && durationEndDate != nil {
		if startDate.Before(*durationStartDate) || startDate.After(*durationEndDate) {
			log.Info("start date not between")
			return nil, nil, &emsError.ResultError{
				ErrorCode: 400,
				Err:       errors.New(fmt.Sprintf("start date must be between %s and %s", durationStartDate.Format("02-01-2006"), durationEndDate.Format("02-01-2006"))),
			}
		}
		if endDate.Before(*durationStartDate) || endDate.After(*durationEndDate) {
			log.Info("end date not between")
			return nil, nil, &emsError.ResultError{
				ErrorCode: 400,
				Err:       errors.New(fmt.Sprintf("end date must be between %s and %s", durationStartDate.Format("02-01-2006"), durationEndDate.Format("02-01-2006"))),
			}
		}
	}
	return &startDate, &endDate, nil
}
