package validator

import (
	emsError "ems/shared/error"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
)

func ApplicationStageValidator(name, location, description *string) error {
	if name == nil || *name == "" {
		log.Info("invalid stage name")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("stage name cannot be null"),
		}
	}
	if location == nil || *location == "" {
		log.Info("invalid stage location")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("stage location cannot be null"),
		}
	}
	if description == nil || *description == "" {
		log.Info("invalid stage description")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("stage description cannot be null"),
		}
	}
	return nil
}
