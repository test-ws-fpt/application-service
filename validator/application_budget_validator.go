package validator

import (
	emsError "ems/shared/error"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
)

func ApplicationBudgetValidator (description *string, unitPrice *float32, amount *uint32, totalPrice *float32) error {
	if description == nil || *description == "" {
		log.Info("invalid budget description")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("budget description cannot be null"),
		}
	}
	if unitPrice == nil {
		log.Info("invalid budget unit price")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("budget unit price cannot be null"),
		}
	}
	if amount == nil {
		log.Info("invalid budget amount")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("budget amount cannot be null"),
		}
	}
	if totalPrice == nil {
		log.Info("invalid budget total price")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("budget total price cannot be null"),
		}
	}
	if *unitPrice * float32(*amount) != *totalPrice {
		log.Info("budget unit price, amount and total price do not match")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("budget unit price, amount and total price do not match"),
		}
	}
	return nil
}
