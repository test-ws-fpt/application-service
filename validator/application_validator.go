package validator

import (
	emsError "ems/shared/error"
	"errors"
	log "github.com/micro/go-micro/v2/logger"
)

func ApplicationValidator(title, description *string, scale *uint32) error {
	if title == nil || *title == "" {
		log.Info("invalid title")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("application title cannot be null"),
		}
	}
	if description == nil || *description == "" {
		log.Info("invalid description")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("application description cannot be null"),
		}
	}
	if scale == nil {
		log.Info("invalid scale")
		return &emsError.ResultError{
			ErrorCode: 400,
			Err:       errors.New("application scale cannot be null"),
		}
	}
	return nil
}