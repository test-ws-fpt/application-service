package injector

import (
	"ems/application-service/facade"
	"github.com/google/wire"
)

var InjectorSet = wire.NewSet(wire.Struct(new(Injector), "*"))


type Injector struct {
	ApplicationFacade facade.IApplicationFacade
	ApplicationApprovalFacade facade.IApplicationApprovalFacade
	ApplicationStageFacade facade.IApplicationStageFacade
	ApplicationContentFacade facade.IApplicationContentFacade
	ApplicationMemberFacade facade.IApplicationMemberFacade
	ApplicationBudgetFacade facade.IApplicationBudgetFacade
}