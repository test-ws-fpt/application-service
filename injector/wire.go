// +build wireinject


package injector

import (
	"github.com/google/wire"
	facade "ems/application-service/facade/impl"
	repository "ems/application-service/repository/mysql"
	service "ems/application-service/service/impl"
)

func BuildInjector() (*Injector, func(), error) {
	wire.Build(
		InitGormDB,
		repository.RepositorySet,
		service.ServiceSet,
		facade.FacadeSet,
		InjectorSet,
	)
	return new(Injector), nil, nil
}