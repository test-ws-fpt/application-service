package facade

import (
	"context"
	"ems/application-service/model"
	application "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
	"time"
)

type IApplicationFacade interface {

	// get application list from an array of id to return data to event service
	GetApplicationsByIds(Ids []uint32) (*application.GetApplicationsFromEventsResponse, error)

	// check user's information and permission to add application, then call AddApplication function from Service layer
	AddApplication(ctx context.Context,
		authSrvClientUserService authPB.UserService,
		authSrvClientRolePermission authPB.RolePermissionService,
		title string, startDate, endDate time.Time, scale uint32, description string) (*model.Application, error)

	// check user's information, then call UpdateApplication function from Service layer
	UpdateApplication(ctx context.Context,
		authSrvClientUserService authPB.UserService,
		id uint32, title string, startDate, endDate time.Time, scale uint32, description string) error

	// check user's information and permission to access an application, then call GetApplicationById function from Service layer
	GetApplicationById(ctx context.Context,
		authSrvClientUserService authPB.UserService,
		authSrvClientRolePermission authPB.RolePermissionService, id uint32) (*application.ViewApplicationDetailsResponse, error)

	// check user's information and permission to show application list depending on that permission,
	// then call GetApplicationList function from Service layer
	GetApplicationList(ctx context.Context,
		authSrvClientUserService authPB.UserService,
		authSrvClientRolePermission authPB.RolePermissionService) (*application.GetApplicationsListResponse, error)
}

type IApplicationBudgetFacade interface {
	// call AddApplicationBudget function from Service layer
	AddApplicationBudget(applicationId uint32, description string, unitPrice float32, amount uint32,
		totalPrice float32) error

	// call UpdateApplicationBudget function from Service layer
	UpdateApplicationBudget(id, applicationId uint32, description string, unitPrice float32, amount uint32,
		totalPrice float32) error
}

type IApplicationMemberFacade interface {
	// call AddApplicationMember function from Service layer
	AddApplicationMember(applicationId, memberId, eventRoleId uint32) error

	// call UpdateApplicationMember function from Service layer
	UpdateApplicationMember(id, applicationId, memberId, eventRoleId uint32) error
}

type IApplicationStageFacade interface {
	// call AddApplicationStage function from Service layer
	AddApplicationStage(applicationId uint32, name, location string, startDate, endDate time.Time,
		description string) (*model.ApplicationStage, error)

	// call UpdateApplicationStage function from Service layer
	UpdateApplicationStage(id, applicationId uint32, name, location string, startDate, endDate time.Time,
		description string) error
}

type IApplicationContentFacade interface {
	// call AddApplicationContent function from Service layer
	AddApplicationContent(applicationStageId uint32, name, description string,
		leaderId uint32, startDate, endDate time.Time) error

	// call UpdateApplicationContent function from Service layer
	UpdateApplicationContent(id, applicationStageId uint32, name, description string,
		leaderId uint32, startDate, endDate time.Time) error
}

type IApplicationApprovalFacade interface {

	// check user permission from Auth service, then call AddApplicationApproval function from Service layer
	AddApplicationApproval(ctx context.Context, authSrvClientUserService authPB.UserService,
		authSrvClientUserAssignEventRoleService authPB.UserAssignEventRoleService,
		authSrvClientRolePermission authPB.RolePermissionService, eventSrvClientEvent eventPB.EventService,
		applicationId, status uint32) error
}