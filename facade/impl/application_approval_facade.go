package impl

import (
	"context"
	"ems/application-service/dto"
	"ems/application-service/facade"
	"ems/application-service/service"
	emsError "ems/shared/error"
	"ems/shared/plugins/broker/rabbitmq"
	authPB "ems/shared/proto/auth"
	eventPB "ems/shared/proto/event"
	"encoding/json"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
)

type ApplicationApprovalFacade struct {
	ApplicationApprovalService service.IApplicationApprovalService
	ApplicationMemberService service.IApplicationMemberService
	ApplicationService service.IApplicationService
}


var _ facade.IApplicationApprovalFacade = (*ApplicationApprovalFacade)(nil)


var ApplicationApprovalFacadeSet = wire.NewSet(wire.Struct(new(ApplicationApprovalFacade), "*"), wire.Bind(new(facade.IApplicationApprovalFacade), new(*ApplicationApprovalFacade)))

func (a *ApplicationApprovalFacade) AddApplicationApproval(ctx context.Context, authSrvClientUserService authPB.UserService, authSrvClientUserAssignEventRoleService authPB.UserAssignEventRoleService, authSrvClientRolePermission authPB.RolePermissionService, eventSrvClientEvent eventPB.EventService, applicationId, status uint32) error {
	permission := true
	permissionRes, err := authSrvClientRolePermission.GetUserPermission(ctx, &authPB.GetUserPermissionRequest{
		PreliminaryApproveApplication:           &permission,
		FinalApproveApplication:    &permission,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if status == 2 && !*permissionRes.PreliminaryApproveApplication {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have the permission")}
	}
	if status == 4 && !*permissionRes.FinalApproveApplication {
		return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have the permission")}
	}
	if status == 5 {
		application, err := a.ApplicationApprovalService.GetApplicationApproval(applicationId)
		if err != nil {
			return err
		}
		if application.StatusCode == 1 && !*permissionRes.PreliminaryApproveApplication {
			return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have the permission")}
		}
		if application.StatusCode == 3 && !*permissionRes.FinalApproveApplication {
			return &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user does not have the permission")}
		}
	}
	authRes, err := authSrvClientUserService.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	err = a.ApplicationApprovalService.AddApplicationApproval(applicationId, status, *authRes.UserId)
	if err != nil {
		return err
	}
	application, err := a.ApplicationService.GetApplicationById(applicationId)
	if err != nil {
		return err
	}
	if status == 4 {
		eventRes, err := eventSrvClientEvent.AddEvent(ctx, &eventPB.AddEventRequest{
			ApplicationId:  &applicationId,
			OrganizationId: authRes.OrganizationId,
		})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event")}
			}
		}
		addEventMemberRequest := authPB.AddEventMemberRequest{
			EventId:        eventRes.EventId,
			OrganizationId: authRes.OrganizationId,
		}
		members, err := a.ApplicationMemberService.GetApplicationMembers(applicationId)
		if err != nil {
			return err
		}
		for _, m := range *members {
			memberId := uint32(m.MemberId)
			eventRoleId := uint32(m.EventRoleId)
			addEventMemberRequest.EventMembers = append(addEventMemberRequest.EventMembers, &authPB.EventMemberDict{
				Id:          &memberId,
				EventRoleId: &eventRoleId,
			})
		}
		_, err = authSrvClientUserAssignEventRoleService.AddEventMember(ctx, &addEventMemberRequest)
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when adding event operation leaders")}
			}
		}
	}
	createdBy := uint32(application.CreatedBy)
	authResUser, err := authSrvClientUserService.GetUserName(ctx, &authPB.GetUserNameRequest{UserId: &createdBy})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err.Error())
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	switch status {
		case 4:
			msg, err := json.Marshal(&dto.ApplicationMessage {
				Name: application.Title,
				Email: *authResUser.Email,
				Status: "Final Approved",
			})
			if err != nil {
				log.Info(err.Error())
			}
			err = rabbitmq.Publish("application_approval_notification", string(msg), 0)
			if err != nil {
				log.Info(err.Error())
			}
		case 5:
			msg, err := json.Marshal(&dto.ApplicationMessage {
				Name: application.Title,
				Email: *authResUser.Email,
				Status: "Rejected",
			})
			if err != nil {
				log.Info(err.Error())
			}
			err = rabbitmq.Publish("application_approval_notification", string(msg), 0)
			if err != nil {
				log.Info(err.Error())
			}
		case 2:
			msg, err := json.Marshal(&dto.ApplicationMessage {
				Name: application.Title,
				Email: *authResUser.Email,
				Status: "Preliminary Approved",
			})
			if err != nil {
				log.Info(err.Error())
			}
			err = rabbitmq.Publish("application_approval_notification", string(msg), 0)
			if err != nil {
				log.Info(err.Error())
			}
			msg, err = json.Marshal(&dto.ApplicationMessage {
				Name: application.Title,
				Email: "hieutthe130168@fpt.edu.vn",
			})
			if err != nil {
				log.Info(err.Error())
			}
			err = rabbitmq.Publish("new_application_notification", string(msg), 0)
			if err != nil {
				log.Info(err.Error())
			}
		case 1:
			msg, err := json.Marshal(&dto.ApplicationMessage {
				Name: application.Title,
				Email: "hoangnvhe130124@fpt.edu.vn",
			})
			if err != nil {
				log.Info(err.Error())
			}
			err = rabbitmq.Publish("new_application_notification", string(msg), 0)
			if err != nil {
				log.Info(err.Error())
			}
	}
	return nil
}