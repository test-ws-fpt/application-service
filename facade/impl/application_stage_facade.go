package impl

import (
	"ems/application-service/facade"
	"ems/application-service/model"
	"ems/application-service/service"
	"github.com/google/wire"
	"time"
)

type ApplicationStageFacade struct {
	ApplicationStageService service.IApplicationStageService
}


var _ facade.IApplicationStageFacade = (*ApplicationStageFacade)(nil)


var ApplicationStageFacadeSet = wire.NewSet(wire.Struct(new(ApplicationStageFacade), "*"), wire.Bind(new(facade.IApplicationStageFacade), new(*ApplicationStageFacade)))

func (a *ApplicationStageFacade) AddApplicationStage(applicationId uint32, name, location string, startDate, endDate time.Time, description string) (*model.ApplicationStage, error) {
	return a.ApplicationStageService.AddApplicationStage(applicationId, name, location, startDate, endDate, description)
}

func (a *ApplicationStageFacade) UpdateApplicationStage(id, applicationId uint32, name, location string, startDate, endDate time.Time, description string) error {
	return a.ApplicationStageService.UpdateApplicationStage(id, applicationId, name, location, startDate, endDate, description)
}