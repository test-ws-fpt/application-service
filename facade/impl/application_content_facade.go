package impl

import (
	"ems/application-service/facade"
	"ems/application-service/service"
	"github.com/google/wire"
	"time"
)

type ApplicationContentFacade struct {
	ApplicationContentService service.IApplicationContentService
}


var _ facade.IApplicationContentFacade = (*ApplicationContentFacade)(nil)


var ApplicationContentFacadeSet = wire.NewSet(wire.Struct(new(ApplicationContentFacade), "*"), wire.Bind(new(facade.IApplicationContentFacade), new(*ApplicationContentFacade)))


func (a *ApplicationContentFacade) AddApplicationContent(applicationStageId uint32, name, description string, leaderId uint32, startDate, endDate time.Time) error {
	return a.ApplicationContentService.AddApplicationContent(applicationStageId, name, description, leaderId, startDate, endDate)
}

func (a *ApplicationContentFacade) UpdateApplicationContent(id, applicationStageId uint32, name, description string, leaderId uint32, startDate, endDate time.Time) error {
	return a.ApplicationContentService.UpdateApplicationContent(id, applicationStageId, name, description, leaderId, startDate, endDate)
}