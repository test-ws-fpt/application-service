package impl

import (
	"context"
	"ems/application-service/facade"
	"ems/application-service/model"
	"ems/application-service/populator"
	"ems/application-service/service"
	emsError "ems/shared/error"
	application "ems/shared/proto/application"
	authPB "ems/shared/proto/auth"
	"errors"
	"github.com/google/wire"
	httpError "github.com/micro/go-micro/v2/errors"
	log "github.com/micro/go-micro/v2/logger"
	"time"
)

type ApplicationFacade struct {
	ApplicationService service.IApplicationService
	ApplicationBudgetService service.IApplicationBudgetService
	ApplicationApprovalService service.IApplicationApprovalService
	ApplicationStageService service.IApplicationStageService
	ApplicationMemberService service.IApplicationMemberService
}


var _ facade.IApplicationFacade = (*ApplicationFacade)(nil)


var ApplicationFacadeSet = wire.NewSet(wire.Struct(new(ApplicationFacade), "*"), wire.Bind(new(facade.IApplicationFacade), new(*ApplicationFacade)))


func (a *ApplicationFacade) GetApplicationList(ctx context.Context, authSrvClientUserService authPB.UserService, authSrvClientRolePermission authPB.RolePermissionService) (*application.GetApplicationsListResponse, error) {
	authRes, err := authSrvClientUserService.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	permission := true
	permissionRes, err := authSrvClientRolePermission.GetUserPermission(ctx, &authPB.GetUserPermissionRequest{
		PreliminaryApproveApplication:           &permission,
		FinalApproveApplication:    &permission,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	permissionId := 3
	if *permissionRes.PreliminaryApproveApplication == true {
		permissionId = 2
	}
	if *permissionRes.FinalApproveApplication == true {
		permissionId = 1
	}
	applicationApprovals, err := a.ApplicationApprovalService.GetApplicationList(*authRes.UserId, *authRes.OrganizationId, uint32(permissionId))
	if err != nil {
		return nil, err
	}
	return populator.GetApplicationListPopulator(applicationApprovals, uint(permissionId)), nil
}

func (a *ApplicationFacade) AddApplication(ctx context.Context, authSrvClientUserService authPB.UserService, authSrvClientRolePermission authPB.RolePermissionService, title string, startDate, endDate time.Time, scale uint32, description string) (*model.Application, error) {
	authRes, err := authSrvClientUserService.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	permission := true
	permissionRes, err := authSrvClientRolePermission.GetUserPermission(ctx, &authPB.GetUserPermissionRequest{
		AddApplication: &permission,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	if *permissionRes.AddApplication {
		return a.ApplicationService.AddApplication(title, startDate, endDate, scale, description, *authRes.UserId, *authRes.OrganizationId)
	} else {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("you do not have the permission")}
	}
}

func (a *ApplicationFacade) UpdateApplication(ctx context.Context, authSrvClientUserService authPB.UserService, id uint32, title string, startDate, endDate time.Time, scale uint32, description string) error {
	authRes, err := authSrvClientUserService.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	return a.ApplicationService.UpdateApplication(id, title, startDate, endDate, scale, description, *authRes.UserId, *authRes.OrganizationId)
}

func (a *ApplicationFacade) GetApplicationById(ctx context.Context, authSrvClientUserService authPB.UserService, authSrvClientRolePermission authPB.RolePermissionService, id uint32) (*application.ViewApplicationDetailsResponse, error) {
	permission := true
	permissionRes, err := authSrvClientRolePermission.GetUserPermission(ctx, &authPB.GetUserPermissionRequest{
		ViewAllPendingApplications: &permission,
		ViewAllApplications: &permission,
	})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user permission")}
		}
	}
	authRes, err := authSrvClientUserService.WhoAmI(ctx, &authPB.WhoAmIRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	application, err := a.ApplicationService.GetApplicationById(id)
	if err != nil {
		return nil, err
	}
	if !*permissionRes.ViewAllApplications && !*permissionRes.ViewAllPendingApplications && *authRes.UserId != uint32(application.CreatedBy) {
		return nil, &emsError.ResultError{ErrorCode: 403, Err: errors.New("this user cannot access this application")}
	}
	applicationApproval, err := a.ApplicationApprovalService.GetApplicationApproval(uint32(application.ID))
	if err != nil {
		return nil, err
	}
	approver := ""
	if applicationApproval.StatusCode == 2 {
		userId := uint32(applicationApproval.ApproverId)
		authRes, err := authSrvClientUserService.GetUserName(ctx, &authPB.GetUserNameRequest{UserId: &userId})
		if err != nil {
			re, ok := err.(*httpError.Error)
			if ok {
				return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
			} else {
				log.Info(err)
				return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
			}
		}
		approver = *authRes.Name
	}
	applicationMembers, err := a.ApplicationMemberService.GetApplicationMembers(uint32(application.ID))
	if err != nil {
		return nil, err
	}
	applicationBudgets, err := a.ApplicationBudgetService.GetApplicationBudgets(uint32(application.ID))
	if err != nil {
		return nil, err
	}
	applicationStages, err := a.ApplicationStageService.GetApplicationStages(uint32(application.ID))
	if err != nil {
		return nil, err
	}
	createdById := uint32(application.CreatedBy)
	authResGetUserName, err := authSrvClientUserService.GetUserName(ctx, &authPB.GetUserNameRequest{UserId: &createdById})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	authResEventRunners, err := authSrvClientUserService.GetEventRunners(ctx, &authPB.GetEventRunnersRequest{})
	if err != nil {
		re, ok := err.(*httpError.Error)
		if ok {
			return nil, &emsError.ResultError{ErrorCode: int(re.Code), Err: errors.New(re.GetDetail())}
		} else {
			log.Info(err)
			return nil, &emsError.ResultError{ErrorCode: 500, Err: errors.New("error when getting user information")}
		}
	}
	createdBy := *authResGetUserName.Name
	res := populator.GetApplicationByIdPopulator(application, applicationMembers, applicationBudgets, applicationStages, applicationApproval, createdBy, approver, authResEventRunners)
	return res, nil
}

func (a *ApplicationFacade) GetApplicationsByIds(Ids []uint32) (*application.GetApplicationsFromEventsResponse, error) {
	applications, err := a.ApplicationService.GetApplicationsByIds(Ids)
	var convertedApplications application.GetApplicationsFromEventsResponse
	for _, a := range *applications {
		var convertedApplication application.GetApplicationsFromEventsDict
		convertedApplication.Title = a.Title
		convertedApplication.Description = a.Description
		convertedApplication.Id = uint32(a.ID)
		convertedApplications.Applications = append(convertedApplications.Applications, &convertedApplication)
	}
	return &convertedApplications, err
}