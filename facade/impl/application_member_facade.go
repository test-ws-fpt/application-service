package impl

import (
	"ems/application-service/facade"
	"ems/application-service/service"
	"github.com/google/wire"
)

type ApplicationMemberFacade struct {
	ApplicationMemberService service.IApplicationMemberService
}


var _ facade.IApplicationMemberFacade = (*ApplicationMemberFacade)(nil)


var ApplicationMemberFacadeSet = wire.NewSet(wire.Struct(new(ApplicationMemberFacade), "*"), wire.Bind(new(facade.IApplicationMemberFacade), new(*ApplicationMemberFacade)))


func (a *ApplicationMemberFacade) AddApplicationMember(applicationId, memberId, eventRoleId uint32) error {
	return a.ApplicationMemberService.AddApplicationMember(applicationId, memberId, eventRoleId)
}

func (a *ApplicationMemberFacade) UpdateApplicationMember(id, applicationId, memberId, eventRoleId uint32) error {
	return a.ApplicationMemberService.UpdateApplicationMember(id, applicationId, memberId, eventRoleId)
}