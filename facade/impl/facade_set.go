package impl

import "github.com/google/wire"

var FacadeSet = wire.NewSet(
	ApplicationFacadeSet,
	ApplicationApprovalFacadeSet,
	ApplicationBudgetFacadeSet,
	ApplicationContentFacadeSet,
	ApplicationMemberFacadeSet,
	ApplicationStageFacadeSet,
	)

