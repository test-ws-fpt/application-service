package impl

import (
	"ems/application-service/facade"
	"ems/application-service/service"
	"github.com/google/wire"
)

type ApplicationBudgetFacade struct {
	ApplicationBudgetService service.IApplicationBudgetService
}

var _ facade.IApplicationBudgetFacade = (*ApplicationBudgetFacade)(nil)


var ApplicationBudgetFacadeSet = wire.NewSet(wire.Struct(new(ApplicationBudgetFacade), "*"), wire.Bind(new(facade.IApplicationBudgetFacade), new(*ApplicationBudgetFacade)))



func (a *ApplicationBudgetFacade) AddApplicationBudget(applicationId uint32, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	return a.ApplicationBudgetService.AddApplicationBudget(applicationId, description, unitPrice, amount, totalPrice)
}

func (a *ApplicationBudgetFacade) UpdateApplicationBudget(id, applicationId uint32, description string, unitPrice float32, amount uint32, totalPrice float32) error {
	return a.ApplicationBudgetService.UpdateApplicationBudget(id, applicationId, description, unitPrice, amount, totalPrice)
}
